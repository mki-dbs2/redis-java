package org.example;

import org.junit.*;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import java.util.*;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class JedisBasicsTest {
    // TODO: Replace the following with your own Redis configuration
    final private static String host = "<your redis host>";
    final private static Integer port = 0; // your port number
    final private static String password = "<your password>";
    final private static Integer timeout = 2000;

    public static String[] testPlanets = { "Mercury", "Mercury", "Venus",
            "Earth", "Earth", "Mars",
            "Jupiter", "Saturn", "Uranus",
            "Neptune", "Pluto" };

    private JedisPoolConfig config;
    private JedisPool pool;
    private Jedis jedis;

    @Before
    public void setUp() {
        this.config = new JedisPoolConfig();

        this.pool = new JedisPool(config, host, port, timeout, password);
        this.jedis = pool.getResource();
        
        jedis.del("planets");
        jedis.del("earth");
    }

    @After
    public void tearDown() {
        jedis.del("planets");
        jedis.del("earth");
        jedis.close();
    }

    @Test
    public void testRedisList() {
        assertThat(testPlanets.length, is(11));

        /* Add all test planets to the Redis set */
        Long result = null; // TODO
        assertThat(result, is(11L));

        // Check the length of the list
        Long length = null; // TODO
        assertThat(length, is(11L));

        // Get the planets from the list
        // Note: LRANGE is an O(n) command. Be careful running this command
        // with high-cardinality sets.
        List<String> planets = null; // TODO
        assertThat(planets, is(Arrays.asList(testPlanets)));

        // Remove the elements that we know are duplicates
        // Note: O(n) operation.
        // TODO
        

        // Drop a planet from the end of the list
        String planet = null; // TODO
        assertThat(planet, is("Pluto"));

        assertThat(jedis.llen("planets"), is(8L));
    }

    @Test
    public void testRedisSet() {
        assertThat(testPlanets.length, is(11));

        // Add all test planets to the Redis set
        // TODO

        // Return the cardinality of the set
        Long length = null; // TODO
        assertThat(length, is(9L));

        // Fetch all values from the set
        // Note: SMEMBERS is an O(n) command. Be careful running this command
        // with high-cardinality sets. Consider SSCAN as an alternative.
        Set<String> planets = null; // TODO

        // Ensure that a HashSet created and stored in Java memory and the set stored
        // in Redis have the same values.
        Set<String> planetSet = new HashSet<>(Arrays.asList(testPlanets));
        assertThat(planets, is(planetSet));

        // Pluto is, of course, no longer a first-class planet. Remove it.
        Long response = null; // TODO
        assertThat(response, is(1L));

        // Now we have 8 planets, as expected.
        Long newLength = null; // TODO
        assertThat(newLength, is(8L));
    }

    @Test
    public void testRedisHash() {
        Map<String, String> earthProperties = new HashMap<>();
        earthProperties.put("diameterKM", "12756");
        earthProperties.put("dayLengthHrs", "24");
        earthProperties.put("meanTempC", "15");
        earthProperties.put("moonCount", "1");

        // Set the fields of the hash one by one.
        for (Map.Entry<String, String> property : earthProperties.entrySet()) {
            // TODO
        }

        // Get the hash we just created back from Redis.
        Map<String, String> storedProperties = null; // TODO
        assertThat(storedProperties, is(earthProperties));

        // Setting fields all at once is more efficient.
        // TODO
        storedProperties = null; // TODO
        assertThat(storedProperties, is(earthProperties));

        // Test that we can get a single property.
        String diameter = null; // TODO
        assertThat(diameter, is(earthProperties.get("diameterKM")));
    }
}
