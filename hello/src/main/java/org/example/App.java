package org.example;

import redis.clients.jedis.*;

public class App {
    // TODO: Replace the following with your own Redis configuration
    final private static String host = "<your redis host>";
    final private static Integer port = 0; // your port number
    final private static String password = "<your password>";
    final private static Integer timeout = 2000;

    public static void main(String[] args) {
        JedisPoolConfig config = new JedisPoolConfig();

        JedisPool pool = new JedisPool(config, host, port, timeout, password);
        Jedis jedis = pool.getResource();
        jedis.set("foo", "bar");
        String value = jedis.get("foo");
        System.out.println(value);
        pool.close();
    }
}


